#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/cudabgsegm.hpp>
#include <opencv2/cudacodec.hpp>
#include <opencv2/cudaobjdetect.hpp>
#include <opencv2/cudaoptflow.hpp>
#include <opencv2/cudastereo.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
using namespace std;
using namespace cv;

Mat img; Mat dst;
const char* filename = "C:\\Users\\Uzzi\\Documents\\YODA\\opencvtest\\n.mp4";
VideoCapture cap(filename);
char window_name[] = "Smoothing Demo";
int main(int argc, char** argv )
{
cuda::printCudaDeviceInfo(0);
    Mat src;  // Capture frame-by-frame
    Mat frame;
    dst = Mat::zeros( src.size(), src.type() );
    int choice;
    printf("Please select your choice: \n");
    cin >> choice;

    if (choice == 1)
    {   int size;
        printf("Processing Median Filter on CPU\n");
        printf("Enter size filter area: \n");
        cin >> size;
        auto start = getTickCount();
        int frames = 0;
        while(1)
        {   auto startV = getTickCount();
            cap >> src;
            if (src.empty())  // If the frame is empty, break immediately
            {break; }
            cvtColor( src, src, cv::COLOR_RGB2GRAY);
            medianBlur(src, dst, size);

            auto endV = getTickCount();
            auto totalTime = (endV - startV) / getTickFrequency();
            auto fps = 1 / totalTime;

            putText(dst, "FPS: " + to_string(int(fps)), Point(50, 50), FONT_HERSHEY_DUPLEX, 1, CV_RGB(0, 0, 250), 2, false);
            imshow( "Frame", dst );  // Display the resulting frame
            frames = frames +1;
            char c=(char)waitKey(1); // Press  ESC on keyboard to exit
        if(waitKey(1) == 'q')
        break;
        }
        auto end = getTickCount();
        printf("Completed total frames of: %i \n", frames);
        auto t = (end - start)/getTickFrequency();
        printf("Total Time Taken = %f \n", t);
        auto FPSAve = frames/t;
        printf("Avarage Frames per Unit Time: %f \n", FPSAve);
    }

    if (choice == 2)
    {
        int low;
        int high;
        printf("Processing Canny Edge Detector on CPU\n");
        printf("Enter Low Threshold? \n");
        cin >> low;
        printf("Enter high Threshold? \n");
        cin >> high;
        auto start = getTickCount();
        int frames = 0;
        while(1)
        {   auto startV = getTickCount();
            cap >> src; // Capture frame-by-frame
            if (src.empty())  // If the frame is empty, break immediately
            {break; }
            cvtColor( src, frame, cv::COLOR_RGB2GRAY);

            cv::Canny(frame, dst, low, high);

            auto endV = getTickCount();
            auto totalTime = (endV - startV) / getTickFrequency();
            auto fps = 1 / totalTime;

            putText(dst, "FPS: " + to_string(int(fps)), Point(50, 50), FONT_HERSHEY_DUPLEX, 1, CV_RGB(0, 0, 250), 2, false);
            imshow( "Frame", dst );  // Display the resulting frame
            frames = frames +1;
            char c=(char)waitKey(1); // Press  ESC on keyboard to exit
        if(waitKey(1) == 'q')
        break;
        }
        auto end = getTickCount();
        printf("Completed total frames of: %i \n", frames);
        auto t = (end - start)/getTickFrequency();
        printf("Total Time Taken = %f \n", t);
        auto FPSAve = frames/t;
        printf("Avarage Frames per Unit Time: %f \n", FPSAve);
    }

    if (choice == 3)
    {
        int size;
        printf("Processing Median Filter on GPU\n");
        printf("Enter size filter area: \n");
        cin >> size;
        cv::cuda::GpuMat imgGPU;
        auto start = getTickCount();
        int frames = 0;
        while(1)
        {   auto startV = getTickCount();
            bool isSuccess = cap.read(src);
            if (src.empty())  // If the frame is empty, break immediately
            {break; }
            
            //Upload to GPU and convert
            
            imgGPU.upload(src);
            cv::cuda::cvtColor(imgGPU, imgGPU, cv::COLOR_RGB2GRAY);
            //imgGPU.convertTo(imgGPU, CV_8UC1);
            cv::Ptr<cv::cuda::Filter> MedianFilter = cv::cuda::createMedianFilter(CV_8UC1, size);
            MedianFilter->apply(imgGPU, imgGPU); //Detect Edges
            cv::Mat dst(imgGPU);

            auto endV = getTickCount();
            auto totalTime = (endV - startV) / getTickFrequency();
            auto fps = 1 / totalTime;
            putText(dst, "FPS: " + to_string(int(fps)), Point(50, 50), FONT_HERSHEY_DUPLEX, 1, CV_RGB(0, 0, 250), 2, false);
            imshow( "Frame", dst );
            frames = frames + 1;
        if(waitKey(1) == 'q')
        break;
        }
        auto end = getTickCount();
        printf("Completed total frames of: %i \n", frames);
        auto t = (end - start)/getTickFrequency();
        printf("Total Time Taken = %f \n", t);
        auto FPSAve = frames/t;
        printf("Avarage Frames per Unit Time: %f \n", FPSAve);
    }

    if (choice == 4)
    {
        int low;
        int high;
        printf("Processing Canny Edge Detector on GPU\n");
        printf("Enter Low Threshold? \n");
        cin >> low;
        printf("Enter high Threshold? \n");
        cin >> high;
        cv::cuda::GpuMat imgGPU;
        cv::cuda::GpuMat imgGPUProc;
        auto start = getTickCount();
        int frames = 0;
        while(1)
        {   auto startV = getTickCount();
            bool isSuccess = cap.read(src);
            if (src.empty())  // If the frame is empty, break immediately
            {break; }
            
            //Upload to GPU and convert
            imgGPU.upload(src);
            cv::cuda::cvtColor(imgGPU, imgGPUProc, cv::COLOR_BGR2GRAY);
            
            cv::Ptr<cv::cuda::CannyEdgeDetector> cannyFilter = cv::cuda::createCannyEdgeDetector(low, high);
            cannyFilter->detect(imgGPUProc, imgGPUProc); //Detect Edges
            cv::Mat dst(imgGPUProc);

            auto endV = getTickCount();
            auto totalTime = (endV - startV) / getTickFrequency();
            auto fps = 1 / totalTime;
            putText(dst, "FPS: " + to_string(int(fps)), Point(50, 50), FONT_HERSHEY_DUPLEX, 1, CV_RGB(0, 0, 250), 2, false);
            imshow( "Frame", dst );  // Display the resulting frame
            frames = frames + 1;

            char c=(char)waitKey(1); // Press  ESC on keyboard to exit
            if(c==27)
            break;
        }
        auto end = getTickCount();
        printf("Completed total frames of: %i \n", frames);
        auto t = (end - start)/getTickFrequency();
        printf("Total Time Taken = %f \n", t);
        auto FPSAve = frames/t;
        printf("Avarage Frames per Unit Time: %f \n", FPSAve);
    }


    //bool check = imwrite("C:\\Users\\Uzzi\\Documents\\YODA\\opencvtest\\results.jpg", dst);
    int c = waitKey();
    return 1;

}